﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateDocuments.aspx.cs" Inherits="CreateDocuments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
   
        <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="Default.aspx">PlottoRace Consultancy</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
    </ul>
      <button class="btn btn-secondary my-2 my-sm-0" type="submit" id="btn_login">Login</button>
  </div>
</nav>
       </div>

    <br />
    <br />
    <h2 style="text-align:center; color:black;">
      <strong>Residential Rent Agreement</strong> 
    </h2>
    <br />
    <br />
    <div style="padding-left:300px; padding-right:300px;">
    <form id="form1" runat="server">
        <label id="label1">Date *</label>
        <br />
       <input type="date" class="form-control" id="date" runat="server" />
        <br />
       <label id="label2">Lessor Name *</label>
        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="LandLord Name"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This is a mandatory field" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
        <br />
        <label id="label3">Lessor Father's Name *</label>
        <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" placeholder="Lessor Father's Name"></asp:TextBox>
        <br />
        <label id="label4">Tennant Name *</label>
        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Proceed to Payment" CssClass="btn btn-primary" />
    </form>
    </div>
     
   

</body>
</html>
