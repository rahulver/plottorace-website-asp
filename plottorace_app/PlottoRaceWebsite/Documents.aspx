﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Documents.aspx.cs" Inherits="Documents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/DocumentsCssPage.css" rel="stylesheet"/>
    <!-- BootStrap Links-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="Default.aspx">PlottoRace Consultancy</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
    </ul>
      <button class="btn btn-secondary my-2 my-sm-0" type="submit" id="btn_login">Login</button>
  </div>
</nav>

  <%--  <!--web contents start here-->
    <div style="background-image: linear-gradient(to top right, darkblue,cornflowerblue); height: 450px;
    width: inherit;">
        
           <h2 style="padding-top:70px; padding-left:100px; padding-right:100px; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif"> Free Legal Forms, Documents and Contracts</h2>

            <form id="form1" runat="server" style="padding-left: 100px; width:900px;">
        <div style="padding-top:100px;">
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Search Documents here like Rent Agreement, NOC, etc"></asp:TextBox>
            <div style="padding-top:20px;">
            <asp:Button ID="Button1" runat="server" Text="Search" CssClass=" btn btn-primary"/>
                </div>
            <asp:Label ID="Label1" runat="server" Text="Label" style="font-size:medium;padding-top:30px; color:white;">Or Scroll down to see list of all the documents.</asp:Label>   
        </div>
    </form>
        </div>--%>

<div class="hero">
     <h2 style="padding-top:70px; padding-left:100px; padding-right:100px; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif"> Free Legal Forms, Documents and Contracts</h2>

            <form id="form1" runat="server" style="padding-left: 100px; width:900px;">
        <div style="padding-top:100px;">
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Search Documents here like Rent Agreement, NOC, etc"></asp:TextBox>
            <div style="padding-top:20px;">
            <asp:Button ID="Button1" runat="server" Text="Search" CssClass=" btn btn-primary"/>
                </div>
            <asp:Label ID="Label1" runat="server" Text="Label" style="font-size:medium;padding-top:30px; color:white;">Or Scroll down to see list of all the documents.</asp:Label>   
        </div>
    </form>
    <svg viewBox="0 0 100 100" preserveAspectRatio="none">
        <polygon points="0,100 100,0 100,100" />
    </svg>
</div>
    <br />
    <br />
    <div style="text-align:center; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size:40px; color:darkblue;">
     <strong> How it Works? </strong>
    </div>
    <br />
    <br />
       <div class="container">
           <div class="row">
               <div class="col-sm">
                   <div class="card">
                       <div class="card-header">
                           <img src="Images/Documents/fill-form.png" style="margin-left:auto; margin-right:auto; display:block; width:40%; height:auto;" />
                       </div>
                       <div class="card-body">
                           <p class="card-text"><strong>Select appropriate form and fill details.</strong></p>
                       </div>
                   </div>
               </div>
               <div class="col-sm">
                    <div class="card">
                       <div class="card-header">
                           <img src="Images/Documents/payment.png" style="margin-left:auto; margin-right:auto; display:block; width:40%; height:auto;" />
                       </div>
                       <div class="card-body">
                           <p style="text-align:center;"><strong>Pay online or download for free.</strong></p>
                       </div>
                   </div>
               </div>
               <div class="col-sm">
                   <div class="card">
                       <div class="card-header">
                           <img src="Images/Documents/delivery.png" style="margin-left:auto; margin-right:auto; display:block; width:40%; height:auto;" />
                       </div>
                       <div class="card-body">
                           <p class="card-text"><strong> Schedule delivery to your doorsteps.</strong></p>
                       </div>
                   </div>
               </div>
           </div>
       </div>
     <br />
    <br />
    <div style="text-align:center; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size:40px; color:darkblue">
     <strong> Documents you can create with us </strong>
    </div>
    <br />
    <br />
    <div class="container">
        <div class="row">
            <div class="col sm">
                <div class="list-group">
  <a href="#" class="list-group-item list-group-item-action" style="background-color:darkorange">
   <h3> Business</h3>
  </a>
  <a href="#" class="list-group-item list-group-item-action">Partnership Agreement
  </a>
  <a href="#" class="list-group-item list-group-item-action">No Objection Certificate
  </a>
  <a href="#" class="list-group-item list-group-item-action">Share Purchase Agreement
  </a>
  <a href="#" class="list-group-item list-group-item-action">Board Resolutions
  </a>
  <a href="#" class="list-group-item list-group-item-action">Invitation Bid Letter
  </a>
</div>
      </div>
             <div class="col sm">
                <div class="list-group">
  <a href="#" class="list-group-item list-group-item-action" style="background-color:darkorange">
   <h3> Real Estate</h3>
  </a>
  <a href="#" class="list-group-item list-group-item-action">Residential Rent Agreement
  </a>
  <a href="#" class="list-group-item list-group-item-action">Commercial Rent Agreement
  </a>
  <a href="#" class="list-group-item list-group-item-action">Notice to pay rent or quit
  </a>
</div>
    </div>
             <div class="col sm">
                <div class="list-group">
  <a href="#" class="list-group-item list-group-item-action" style="background-color:darkorange">
   <h3> Emloyment</h3>
  </a>
  <a href="#" class="list-group-item list-group-item-action">Employment Offer Letter
  </a>
  <a href="#" class="list-group-item list-group-item-action">Employment Warning Letter
  </a>
  <a href="#" class="list-group-item list-group-item-action">Appointment Letter
  </a>
</div>
    </div>
   </div>
    </div>
    <br />
    <br />
    <div class="container">
        <div class="row">
                   <div class="col sm">
                <div class="list-group">
  <a href="#" class="list-group-item list-group-item-action" style="background-color:cadetblue">
   <h3> Startup</h3>
  </a>
  <a href="#" class="list-group-item list-group-item-action">Website Maintenance Agreement 
  </a>
  <a href="#" class="list-group-item list-group-item-action">Website Development Agreement 
  </a>
  <a href="#" class="list-group-item list-group-item-action">Breach of Contract Notice
  </a>
  <a href="#" class="list-group-item list-group-item-action">Copyright Assignment Agreement
  </a>
</div>
    </div>
        </div>

    </div>
</body>
</html>
