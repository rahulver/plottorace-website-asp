﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Patent.aspx.cs" Inherits="Patent" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in Hyderabad.">
    <meta name="author" content="Best Legal Service Provider in New Delhi">
    <meta name="author" content="Best Legal Service Provider in Hajipur.">
    <meta name="author" content="Best Legal Service Provider in Kolkata.">
    <meta name="author" content="Company Secretaries in Hyderabad.">
    <meta name="author" content="Company Secretaries in New Delhi.">
    <meta name="author" content="Company Secretaries in India.">
    <meta name="author" content="Company Secretaries in Kolkata.">
    <meta name="author" content="Company Secretaries in Hajipur.">

    <!-- Page Title -->
    <title> PlottoRace Consultancy- Best Corporate Legal Service Provider in India. </title>

    <!-- Favicon and Touch Icons -->
    <link href="assets/images/favicon/favicon.png" rel="shortcut icon" type="image/png">
    <link href="assets/images/favicon/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
    <link href="assets/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="assets/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="assets/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icon fonts -->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/flaticon.css" rel="stylesheet" />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" />

    <!-- Plugins for this template -->
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/owltheme.css" rel="stylesheet" />
    <link href="css/slick.css" rel="stylesheet" />
    <link href="css/slick-theme.css" rel="stylesheet" />
    <link href="css/owl-transition.css" rel="stylesheet" />
    <link href="css/fancybox.css" rel="stylesheet" />
    <link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/StyleSheet1.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet" />
</head>

<body>

        <!-- Start header -->
        <header id="header" class="site-header header-style-1">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-sm-6">
                            <ul class="contact-info">
                                <li><i class="fa fa-phone-square"></i> Phone: +91 8800435387</li>
                                <li><i class="fa fa-clock-o"></i> Mon - Fri: 9 am - 7 pm</li>
                            </ul>
                        </div>
                        <div class="col col-sm-6">
                            <div class="language">
                                <span><i class="fa fa-globe"></i> Lang:</span>
                                <div class="select-box">
                                    <select class="selectpicker" id="language-select">
                                        <option>Eng</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> <!-- end topbar -->
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand">
                            <img src="Images/Logo_.jpg" height="75" width="250" /></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">

                             <li><a href="Default.aspx">Home</a></li>

                            <li><a href="#">About</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Services</a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Start Your own Business</a>
                                        <ul class="sub-menu">
                                            <li><a href="OPC.aspx">Registration of One Person Company(OPC)</a></li>
                                            <li><a href="LLP.aspx">Registration of LLP</a></li>
                                            <li><a href="PublicLimitedCompany.aspx">Registration of Public Limited Company </a></li>
                                            <li><a href="PrivateLimitedCompany.aspx">Registration of Private Limited Company</a></li>
                                            <li><a href="Section8.aspx">Registration of Section 8</a></li>

                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Company Law</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">ROC/Annual filing</a></li>
                                            <li><a href="#">Director Appointment/Resignation</a></li>
                                            <li><a href="#">Increase in Share Capital </a></li>
                                            <li><a href="#">Alteration of MOA</a></li>
                                            <li><a href="#">Name Change</a></li>
                                            <li><a href="#">Minutes Book</a></li>
                                            <li><a href="#">Share Transfer</a></li>
                                            <li><a href="#">Share Certificate </a></li>
                                            <li><a href="#">Share Holder Register</a></li>
                                            <li><a href="#">Registered Office Change</a></li>
                                            <li><a href="#">Charge Creation/Satisfaction and Modification</a></li>
                                            <li><a href="#">Strike Off</a></li>fd

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Taxtion</a>
                                <ul class="sub-menu">
                                    <li><a href="Income_Tax_Return.html">Income Tax Return</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">GST</a>
                                        <ul class="sub-menu">
                                            <li><a href="GST.aspx">GST</a></li>
                                            <li><a href="GSTDocuments.aspx">GST DOC</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="Other_Tax_compliances.html">Other Tax compliances</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">IPR</a>
                                <ul class="sub-menu">
                                    <li><a href="Trademark.aspx">Trademark</a></li>
                                    <li><a href="Copyright.aspx">Copyright</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Other Services</a>
                                <ul class="sub-menu">
                                    <li><a href="#">ISO Certification</a></li>
                                    <li><a href="MSME.aspx">MSME/SSI Registration</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                    <div class="search-social">
                        <div class="header-search-area">
                            <div class="header-search-form">
                                <%--<form class="form">
                                    <div>
                                        <input type="text" class="form-control" placeholder="Search here">
                                    </div>
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>--%>
                            </div>
                            <div>
                                <button class="btn open-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
    <!--start of content here-->
    <div style="position:relative; text-align:center; color:white;">
    <img src="Images/Services/patent.jpg" height="250" width="1550" />
<%--<div style="position:absolute; top:50%; left:50%; transform: translate(-50%, -50%); font-size:60px; color:black;"><b>Private Limited Company</b></div>--%>--%>
    </div>
    <div style="background-color:white;">
        <div class="heading">
            <b> Patent</b> 
        </div>
            <div class="heading-content">
                <div class="heading-content">
                    <h3> <b style="color:green;">Definition and Significance</b></h3>
                    <h4>What is Patent?</h4>
                    <p style="text-align:justify;">A patent is granted for an invention which is a new product or process involving an inventive step and capable of industrial application. “New invention" means the subject matter has not fallen in public domain or that it does not form part of the state of the art; Inventive step is the feature(s) of the invention that involves technical advance as compared to the existing knowledge or having economic significance or both and that makes the invention not obvious to a person skilled in the art. Capable of Industrial application means that the invention is capable of being made or used in an industry.</p>
                    <h4>MINISTRY/OFFICE ADMINISTERING THE IPR:</h4>
                    <ul>
                        <li>Department of Industrial Policy and Promotion, Ministry of Commerce & Industry</li>
                        <li>Controller General of Patents, Designs and Trade Marks</li>
                        <li>Concerned IP Act</li>
                        <li>The Patents Act, 1970 (as amended in 2005)</li>
                        </ul>
                     <h3><b style="color:green;">Documents Required for filing Patent:</b></h3>
                
                    <ul style="text-align:justify;">
                       <li>Patent application in Form-1.
                        </li>
                        
                        <li>Proof of right to file application from the inventor. The proof of cight can either be an endorsement at the end of the application or a separate agreement attached with the patent application.
                        </li>
                        <li>Provisional specifications, if complete specifications are not available.
                        </li>
                        <li>
                            Complete specification in Form-2 within 12 months of filing of provisional specification.
                        </li>
                        <li>Statement and undertaking under Section 8 in Form- 3, if applicable. Form 3 can be filed along with the application or within 6 months from the date of application.
                            </li>
                        <li>Declaration as to inventorship in Form 5 for applications with complete specification or a convention application or a PCT application designating India. Form-5 or Declaration as to inventorship can be filed within one month from the date of filing of application, if a request is made to the Controller in Form-4.</li>
                        <li>Power of authority in Form-26, if patent application is being filed by a Patent Agent. In case a general power of authority, then a self attested copy of the same can be filed by the Patent Agent or Patent Attorney.</li> 
                         <li>If the Application pertains to a biological material obtained from India, the applicant is required to submit the permission from the National Biodiversity Authority any time before the grant of the patent. However, it is sufficient if the permission from the National Biodiversity Authority is submitted before the grant of the patent.</li> 
                         <li>The Application form should also indicate clearly the source of geographical origin of any biological material used in the specification.</li> 
                         <li>All patent applications must bear the signature of the applicant or authorized person or Patent Attorney along with name and date.</li> 
                         <li>Provisional or complete specification must be signed by the agent/applicant with date on the last page of the specification. The drawing sheets attached should also contain the signature of an applicant or his agent in the right hand bottom corner.</li> 
                         <li>Priority document must be filed in the following cases:</li> 
                       
                        </ul>
                    <ul>
                        <li>Convention Application (under Paris Convention).</li>
                        <li>PCT National Phase Application wherein requirements of Rule 17.1(a or b) of hasnot been fulfilled.</li>
                        <li>Note: Priority document must be filed along with the application or before the expiry of eighteen months from the date of priority, to enable early publication of the application.</li>
                    </ul>
                    </div>
                </div>
        </div>
                  
    <br/>
    <br/>
       

        <!-- start site-footer -->
        <footer class="site-footer">
            <div class="upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-4 col-sm-6">
                            <div class="widget about-widget">
                                <div class="footer-logo"><img src="assets/images/footer-logo.png" alt></div>
                                <p>We are one of the Indis's leading corporate consultancy firm having experience of serving more than 100+ corporate clients.</p>
                                <ul class="contact-info">
                                    <li><i class="fa fa-phone"></i> +91 8800435387 </li>
                                    <li><i class="fa fa-envelope"></i> info@plottorace.com</li>
                                    <li><i class="fa fa-home"></i> Street No 22, Vipin Garden Extension, Dwarka Mor, New Delhi</li>
                                    <li><i class="fa fa-home"></i> Startx Co-working Space, Opposite Dlf Cyber City Gate No. 2, Gachibowli, Hyderabad</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-2 col-sm-6">
                            <div class="widget links-widget">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">OPC</a></li>
                                    <li><a href="#">LLP</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-3 col-sm-6">
                            <div class="widget support-widget">
                                <h3>Support</h3>
                                <ul>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Free Consultation with CA</a></li>
                                    <li><a href="#">Free consultation with CS</a></li>
                                    <li><a href="#">Free Consultation with Lawyer</a></li>
      
                                    <li><a href="#">Professional Services</a></li>
                                </ul>
                            </div>
                        </div>

                      
            <div class="copyright-info">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="copyright-area">
                             
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="footer-social">
                                <span>Follow us: </span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->        
    <!--</div>-->
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="Javascript/jquery.min.js"></script>
    <script src="Javascript/bootstrap.js"></script>

    <!-- Plugins for this template -->
    <script src="Javascript/jquery-plugin-collection.js"></script>

    <!-- Google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key"></script>

    <!-- Custom script for this template -->
    <script src="Javascript/script.js"></script>
</body>
</html>



