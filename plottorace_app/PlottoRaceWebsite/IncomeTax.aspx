﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IncomeTax.aspx.cs" Inherits="IncomeTax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="Best Legal Service Provider in India."/>
    <meta name="author" content="Best Legal Service Provider in India."/>
    <meta name="author" content="Best Legal Service Provider in Hyderabad."/>
    <meta name="author" content="Best Legal Service Provider in New Delhi"/>
    <meta name="author" content="Best Legal Service Provider in Hajipur."/>
    <meta name="author" content="Best Legal Service Provider in Kolkata."/>
    <meta name="author" content="Company Secretaries in Hyderabad."/>
    <meta name="author" content="Company Secretaries in New Delhi."/>
    <meta name="author" content="Company Secretaries in India."/>
    <meta name="author" content="Company Secretaries in Kolkata."/>
    <meta name="author" content="Company Secretaries in Hajipur."/>

    <!-- Page Title -->
    <title> PlottoRace Consultancy- Best Corporate Legal Service Provider in India. </title>

    <!-- Favicon and Touch Icons -->
    <link href="assets/images/favicon/favicon.png" rel="shortcut icon" type="image/png"/>
    <link href="assets/images/favicon/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
    <link href="assets/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
    <link href="assets/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
    <link href="assets/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <!-- Icon fonts -->
    <link href="css/font-awesome.css" rel="stylesheet"/>
    <link href="css/flaticon.css" rel="stylesheet"/>
    <link href="css/IncomeTaxStyles.css" rel="stylesheet" />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet"/>

    <!-- Plugins for this template -->
    <link href="css/animate.css" rel="stylesheet"/>
    <link href="css/caraousel.css" rel="stylesheet"/>
    <link href="css/owltheme.css" rel="stylesheet"/>
    <link href="css/slick.css" rel="stylesheet"/>
    <link href="css/slick-theme.css" rel="stylesheet"/>
    <link href="css/owl-transitions.css" rel="stylesheet"/>
    <link href="css/fancybox.css" rel="stylesheet"/>
    <link href="css/bootstrap-select.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <!-- start preloader -->
        <div class="preloader">
            <div class="inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- end preloader -->
     <!-- Start header -->
        <header id="header" class="site-header header-style-1">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-sm-6">
                            <ul class="contact-info">
                                <li><i class="fa fa-phone-square"></i> Phone: +91 8800435387</li>
                                <li><i class="fa fa-clock-o"></i> Mon - Fri: 9 am - 7 pm</li>
                            </ul>
                        </div>
                        <div class="col col-sm-6">
                            <div class="language">
                                <span><i class="fa fa-globe"></i> Lang:</span>
                                <div class="select-box">
                                    <select class="selectpicker" id="language-select">
                                        <option>Eng</option>
                                        <option>Ban</option>
                                        <option>Tur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> <!-- end topbar -->
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand">
                            <img src="Images/Logo_.jpg" height="75" width="250" /></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">

                            <li><a href="Default.aspx">Home</a></li>

                            <li><a href="#">About</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Services</a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Start Your own Business</a>
                                        <ul class="sub-menu">
                                            <li><a href="OPC.aspx">Registration of One Person Company(OPC)</a></li>
                                            <li><a href="LLP.aspx">Registration of LLP</a></li>
                                            <li><a href="PublicLimitedCompany.aspx">Registration of Public Limited Company </a></li>
                                            <li><a href="PrivateLimitedCompany.aspx">Registration of Private Limited Company</a></li>
                                            <li><a href="Section8.aspx">Registration of Section 8</a></li>

                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Company Law</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">ROC/Annual filing</a></li>
                                            <li><a href="#">Director Appointment/Resignation</a></li>
                                            <li><a href="#">Increase in Share Capital </a></li>
                                            <li><a href="#">Alteration of MOA</a></li>
                                            <li><a href="#">Name Change</a></li>
                                            <li><a href="#">Minutes Book</a></li>
                                            <li><a href="#">Share Transfer</a></li>
                                            <li><a href="#">Share Certificate </a></li>
                                            <li><a href="#">Share Holder Register</a></li>
                                            <li><a href="#">Registered Office Change</a></li>
                                            <li><a href="#">Charge Creation/Satisfaction and Modification</a></li>
                                            <li><a href="#">Strike Off</a></li>fd

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Taxtion</a>
                                <ul class="sub-menu">
                                    <li><a href="Income_Tax_Return.html">Income Tax Return</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">GST</a>
                                        <ul class="sub-menu">
                                            <li><a href="GST.aspx">GST</a></li>
                                            <li><a href="GSTDocuments.aspx">GST DOC</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="Other_Tax_compliances.html">Other Tax compliances</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">IPR</a>
                                <ul class="sub-menu">
                                    <li><a href="Trademark.aspx">Trademark</a></li>
                                    <li><a href="Copyright.aspx">Copyright</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Other Services</a>
                                <ul class="sub-menu">
                                    <li><a href="#">ISO Certification</a></li>
                                    <li><a href="MSME.aspx">MSME/SSI Registration</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                           <li><a href="ContactUs.aspx">Contact</a></li>
                    </div><!-- end of nav-collapse -->
                    <div class="search-social">
                        <div class="header-search-area">
                            <div class="header-search-form">
                                <form class="form">
                                    <div>
                                        <input type="text" class="form-control" placeholder="Search here">
                                    </div>
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <div>
                                <button class="btn open-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
    <div>
        <img src="Images/income-tax.jpeg" height="250" width="1550" />
    </div>
    
        <div class="sideleft">
             <h1 style="text-align:center">
             <strong>   Income Tax in India</strong>
            </h1>
            <div style="padding-left:20px; text-align:justify;">
                 <br/>
        
                <h2> What is Income Tax?</h2>
               
        <h4>   Income Tax refers to the tax that you pay directly to the government depending upon your income or profit. In India, Taxes levied by the government is of two types: Direct Taxes and Indirect Taxes.
           
            </h4>
              <h4> Indirect Taxes are those that are levied on services and goods. Direct taxes, on the other hand, are levied on profits and incomes. For example, service tax is what you pay in restaurant and is an indirect tax, whereas Income Tax that is deducted from your salary every month in the form of TDS, is an example of direct tax.</h4>  
          <br/>
                  
                <h2> Who are the Tax payers?</h2>
                <h4>
                    Any Indian citizen aged below 60 years is liable to pay income tax, is their income exceeds 2.5 lakhs. If the individual is above 60 years of age and earns more than 2.5 lakhs, he/she will have to pay taxes to the Government of India. Additionally, the following entities that generate income are liable pay taxes:
                  
                </h4>
                  <ul>
                        <li>Hindu Undivided Family(HUF)</li>
                      <li>Body of Individuals(BOI)</li>
                      <li>Associations of Persons(AOP)</li>
                      <li>Local Authorities</li>
                      <li>Corporate Firms</li>
                      <li>Companies</li>
                      <li>
                          All Artificial Juridicial Persons
                      </li>
                    </ul>
                <br />
                
                <h3>What are the Income Tax Slab Rates?</h3>
              
                <h4>Income tax slab for individual male and female tax payers of age less than 60 years and HUF FY 2018-19</h4>
<table class="table table-curved doc-table">
<thead>
<tr>
<th class="th width25">Income range per annum</th>
<th class="th">Tax Rate FY 2018-19, AY 2019-20</th>
<th class="th">Tax Rate FY 2017-18, AY 2018-19</th>
</tr>
</thead>
<tbody>
  <tr>
    <td>Up to Rs. 2.5 lakhs</td>
<td>No Tax</td>
<td>No Tax</td>
</tr>
<tr>
<td>Above Rs. 2.5 lakhs to Rs. 5 lakhs</td>
<td>5% + 4% cess</td>
<td>5% + 3% cess</td>
</tr>
<tr>
<td>Above Rs. 5 lakhs to Rs. 10 lakhs</td>
<td>20% + 4% cess</td>
<td>20% + 3% cess</td>
</tr>
<tr>
<td>Above Rs. 10 lakhs to Rs. 50 lakhs</td>
<td>30% + 4% cess</td>
<td>30% + 3% cess</td>
</tr>
<tr>
<td>Above Rs. 50 lakhs to Rs. 1 crore</td>
<td>30% + 10% surcharge + 4% cess</td>
<td>30% + 10% surcharge + 3% cess</td>
</tr>
<tr>
<td>Above Rs. 1 crore</td>
<td>30% +15% surcharge + 4% cess</td>
<td>30% +15% surcharge + 3% cess</td>
</tr>
<tr>
<td>Rebate under section 87(A)</td>
<td>100% tax rebate subject to maximum of Rs. 2,500 available to resident individual whose total income does not exceed Rs. 3.5 lakhs</td>
<td>100% tax rebate subject to maximum of Rs. 2,500 available to resident individual whose total income does not exceed Rs. 3.5 lakhs</td>
</tr>
</tbody>
</table>
                <br />

 <h3>Income tax slabs for senior citizens including both male and female taxpayers of age more than 60 years but less than 80 years FY 2018-19</h3>
<table class="table table-curved doc-table">
<thead>
<tr>
<th class="th" width="25%">Income range per annum</th>
<th class="th">Tax Rate FY2018-19, AY 2019-20</th>
<th class="th">Tax Rate FY2017-18, AY 2018-19</th>
</tr>
</thead>
<tbody>
<td>Up to Rs. 3 lakhs</td>
<td>No Tax</td>
<td>No Tax</td>
</tr>

<tr>
<td>Above Rs. 3 lakh to Rs. 5 lakhs </td>
<td>5% + 4% cess</td>
<td>5% + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 5 lakhs to Rs. 10 lakhs</td>
<td>20% + 4% cess</td>
<td>20% + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 10 lakhs to Rs. 50 lakhs</td>
<td>30% + 4% cess</td>
<td>30% + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 50 lakhs to Rs. 1 crore </td>
<td>30% + 10% surcharge + 4% cess</td>
<td>30% + 10% surcharge + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 1 crore</td>
<td>30% +15% surcharge + 4% cess</td>
<td>30% +15% surcharge + 3% cess</td>
</tr>

<tr>
<td>Rebate under section 87(A)</td>
<td>100% tax rebate subject to maximum of Rs. 2,500 available to resident individual whose total income does not exceed Rs. 3.5 lakhs</td>
<td>100% tax rebate subject to maximum of Rs. 2,500 available to resident individual whose total income does not exceed Rs. 3.5 lakhs</td>
</tr>

</tbody>
</table>

<br />
<h3>Income tax slab for super senior citizen including both male and female taxpayers of age more than 80 years FY 2018-19</h3>
<table class="table table-curved doc-table">
<thead>
<tr>
<th class="th" width="25%">Income range per annum</th>
<th class="th">Tax Rate FY 2018-19, AY 2019-20</th>
<th class="th">Tax Rate FY 2017-18, AY 2018-19</th>
</tr>
</thead>
<tbody><tr>
<td>Up to Rs. 2.5 lakhs per annum</td>
<td>No Tax</td>
<td>No Tax</td>
</tr>

<tr>
<td>Up to Rs. 5 lakhs per annum</td>
<td>No Tax</td>
<td>No Tax</td>
</tr>

<tr>
<td>Above Rs. 5 lakhs to Rs. 10 lakhs </td>
<td>20% + 4% cess</td>
<td>20% + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 10 lakhs to Rs. 50 lakhs</td>
<td>30% + 4% cess</td>
<td>30% + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 50 lakhs to Rs. 1 crore</td>
<td>30% + 10% surcharge + 4% cess</td>
<td>30% + 10% surcharge + 3% cess</td>
</tr>

<tr>
<td>Above Rs. 1 crore</td>
<td>30% +15% surcharge + 4% cess</td>
<td>30% +15% surcharge + 3% cess</td>
</tr>

</tbody>
</table>

                <br />
                <h3>Tax slabs for Firms and Domestic Companies FY 2018-19</h3>
<table class="table table-curved doc-table">
<thead>
<tr>
<th class="th">Tax Head</th>
<th class="th">Firms</th>
<th class="th">Domestic Companies</th>
</tr>
</thead>
<tbody>

<tr>
<td>Income Tax for turnover upto Rs. 50 crores</td>
<td>30%</td>
<td>25%</td>
</tr>

<tr>
<td>Income Tax for turnover above Rs. 50 crores</td>
<td>30%</td>
<td>30%</td>
</tr>

<tr>
<td>Surcharge as % of income tax</td>
<td>12% of tax in case the total income exceeds Rs. 1 crore</td>
<td><ul><li>7% of tax in case income is more than Rs. 1 crore but less than Rs. 10 crore.</li> 
<li>10% of tax in case income is more than Rs. 10 crore.</li></ul></td>
</tr>
<tr>
<td>Cess as % of tax and surcharge</td>
<td>3% of tax plus surcharge</td>
<td>3% of tax plus surcharge</td>
</tr>
</tbody>
</table>



            </div>
            <div class="sideright">
        <h2>Get Your ITR Filed with us</h2>
    </div>
        <form id="form1" runat="server">
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Name is Required" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" placeholder="Mobile Number"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Mobile Number" ControlToValidate="TextBox2" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Email " ControlToValidate="TextBox3" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" placeholder="Monthly Income"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Monthly Income is Required" ControlToValidate="TextBox4"></asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
    </form>
    </div>
    
      <!-- start site-footer -->
        <footer class="site-footer">
            <div class="upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-4 col-sm-6">
                            <div class="widget about-widget">
                                <div class="footer-logo"><img src="assets/images/footer-logo.png" alt></div>
                                <p>We are one of the Indias's leading corporate consultancy firm having experience of serving more than 100+ corporate clients.</p>
                                <ul class="contact-info">
                                    <li><i class="fa fa-phone"></i> +91 8800435387 </li>
                                    <li><i class="fa fa-envelope"></i> info@plottorace.com</li>
                                    <li><i class="fa fa-home"></i> Street No 22, Vipin Garden Extension, Dwarka Mor, New Delhi</li>
                                    <li><i class="fa fa-home"></i> Startx Co-working Space, Opposite Dlf Cyber City Gate No. 2, Gachibowli, Hyderabad</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-2 col-sm-6">
                            <div class="widget links-widget">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="Default.aspx">Home</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="About.aspx">About us</a></li>
                                    <li><a href="OPC.aspx">OPC</a></li>
                                    <li><a href="LLP.aspx">LLP</a></li>
                                    <li><a href="ContactUs.aspx">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-3 col-sm-6">
                            <div class="widget support-widget">
                                <h3>Support</h3>
                                <ul>
                                    <li><a href="ContactUs.aspx">Contact Us</a></li>
                                    <li><a href="ContactUs.aspx">Free Consultation with CA</a></li>
                                    <li><a href="ContactUs.aspx">Free consultation with CS</a></li>
                                    <li><a href="ContactUs.aspx">Free Consultation with Lawyer</a></li>
      
                                    <li><a href="ContactUs.aspx">Professional Services</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                       
            <div class="copyright-info">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="copyright-area">
                             
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="footer-social">
                                <span>Follow us: </span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->        
    <!--</div>-->
   


    <!-- All JavaScript files
    ================================================== -->
    <script src="Javascript/jquery.min.js"></script>
    <script src="Javascript/bootstrap.js"></script>

    <!-- Plugins for this template -->
    <script src="Javascript/jquery-plugin-collection.js"></script>

    <!-- Google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key"></script>

    <!-- Custom script for this template -->
    <script src="Javascript/script.js"></script>
</body>
</html>
