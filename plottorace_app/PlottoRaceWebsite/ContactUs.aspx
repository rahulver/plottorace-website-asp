﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in Hyderabad.">
    <meta name="author" content="Best Legal Service Provider in New Delhi">
    <meta name="author" content="Best Legal Service Provider in Hajipur.">
    <meta name="author" content="Best Legal Service Provider in Kolkata.">
    <meta name="author" content="Company Secretaries in Hyderabad.">
    <meta name="author" content="Company Secretaries in New Delhi.">
    <meta name="author" content="Company Secretaries in India.">
    <meta name="author" content="Company Secretaries in Kolkata.">
    <meta name="author" content="Company Secretaries in Hajipur.">

    <!-- Page Title -->
    <title> PlottoRace Consultancy- Best Corporate Legal Service Provider in India. </title>

    <!-- Favicon and Touch Icons -->
    <link href="assets/images/favicon/favicon.png" rel="shortcut icon" type="image/png">
    <link href="assets/images/favicon/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
    <link href="assets/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="assets/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="assets/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icon fonts -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Plugins for this template -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/caraousel.css" rel="stylesheet">
    <link href="css/owltheme.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/slick-theme.css" rel="stylesheet">
    <link href="css/owl-transitions.css" rel="stylesheet">
    <link href="css/fancybox.css" rel="stylesheet">
    <link href="css/bootstrap-select.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <!-- start page-wrapper -->
    <!--<div class="page-wrapper">-->

        <!-- start preloader -->
        <div class="preloader">
            <div class="inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- end preloader -->

        <!-- Start header -->
        <header id="header" class="site-header header-style-1">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-sm-6">
                            <ul class="contact-info">
                                <li><i class="fa fa-phone-square"></i> Phone: +91 8800435387</li>
                                <li><i class="fa fa-clock-o"></i> Mon - Fri: 9 am - 7 pm</li>
                            </ul>
                        </div>
                        <div class="col col-sm-6">
                            <div class="language">
                                <span><i class="fa fa-globe"></i> Lang:</span>
                                <div class="select-box">
                                    <select class="selectpicker" id="language-select">
                                        <option>Eng</option>
                                        <option>Ban</option>
                                        <option>Tur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> <!-- end topbar -->
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand">
                            <img src="Images/Logo_.jpg" height="75" width="250" /></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">

                            <li><a href="Default.aspx">Home</a></li>

                            <li><a href="#">About</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Services</a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Start Your own Business</a>
                                        <ul class="sub-menu">
                                            <li><a href="OPC.aspx">Registration of One Person Company(OPC)</a></li>
                                            <li><a href="LLP.aspx">Registration of LLP</a></li>
                                            <li><a href="PublicLimitedCompany.aspx">Registration of Public Limited Company </a></li>
                                            <li><a href="PrivateLimitedCompany.aspx">Registration of Private Limited Company</a></li>
                                            <li><a href="Section8.aspx">Registration of Section 8</a></li>

                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Company Law</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">ROC/Annual filing</a></li>
                                            <li><a href="#">Director Appointment/Resignation</a></li>
                                            <li><a href="#">Increase in Share Capital </a></li>
                                            <li><a href="#">Alteration of MOA</a></li>
                                            <li><a href="#">Name Change</a></li>
                                            <li><a href="#">Minutes Book</a></li>
                                            <li><a href="#">Share Transfer</a></li>
                                            <li><a href="#">Share Certificate </a></li>
                                            <li><a href="#">Share Holder Register</a></li>
                                            <li><a href="#">Registered Office Change</a></li>
                                            <li><a href="#">Charge Creation/Satisfaction and Modification</a></li>
                                            <li><a href="#">Strike Off</a></li>fd

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Taxtion</a>
                                <ul class="sub-menu">
                                    <li><a href="Income_Tax_Return.html">Income Tax Return</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">GST</a>
                                        <ul class="sub-menu">
                                            <li><a href="GST.aspx">GST</a></li>
                                            <li><a href="GSTDocuments.aspx">GST DOC</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="Other_Tax_compliances.html">Other Tax compliances</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">IPR</a>
                                <ul class="sub-menu">
                                    <li><a href="Trademark.aspx">Trademark</a></li>
                                    <li><a href="Copyright.aspx">Copyright</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Other Services</a>
                                <ul class="sub-menu">
                                    <li><a href="#">ISO Certification</a></li>
                                    <li><a href="MSME.aspx">MSME/SSI Registration</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                           <li><a href="ContactUs.aspx">Contact</a></li>
                    </div><!-- end of nav-collapse -->
                    <div class="search-social">
                        <div class="header-search-area">
                            <div class="header-search-form">
                                <form class="form">
                                    <div>
                                        <input type="text" class="form-control" placeholder="Search here">
                                    </div>
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <div>
                                <button class="btn open-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                           
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
     <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Contact</h2>
                        <ol class="breadcrumb">
                            <li><a href="Default.aspx">Home</a></li>
                            <li>Contact</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>        
        <!-- end page-title -->


        <!-- start contact-pg-content -->
        <section class="contact-pg-content section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-8 col-md-offset-2">
                        <div class="section-title-s3">
                            <h2>Let’s Get In Touch</h2>
                            <p>We will get back to you within 3 working days.</p>
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>                
                <div class="row">
                    <div class="col col-md-6">
                        <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=gali%20no%2022%20vipin%20garden%20extension&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.emojilib.com"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:550px;}</style></div>
                    </div>
                    <div class="col col-md-6">
                        <div class="contact-form"> 
                            <form runat="server" id="contactform" class="form row contact-validation-active">
                                <div class="col col-xs-12">
                                    
                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeholder="Name" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Name is Mandatory" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col col-xs-12">
                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" placeholder="Email" Width="100%"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid Email" ControlToValidate="TextBox2" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col col-xs-12">
                                    <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" placeholder="Mobile" Width="100%"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid Mobile No." ControlToValidate="TextBox3" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col col-xs-12">
                                    <asp:TextBox ID="TextBox4" runat="server" Rows="25" CssClass="form-control" placeholder="Message here..." Width="100%" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Message is Mandatory" ControlToValidate="TextBox4"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col col-xs-12 submit-btn">
                                    <asp:Button ID="Button1" runat="server" Text="Submit"  CssClass="form-control" OnClick="btnSubmit_Click"/>
                                    <div id="loader">
                                        <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                                    </div>
                                </div>
                                <%--<div class="error-handling-messages">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>--%>
                            </form>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end contact-pg-content -->


    <!-- start site-footer -->
        <footer class="site-footer">
            <div class="upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-4 col-sm-6">
                            <div class="widget about-widget">
                                <div class="footer-logo"><img src="assets/images/footer-logo.png" alt></div>
                                <p>We are one of the Indis's leading corporate consultancy firm having experience of serving more than 100+ corporate clients.</p>
                                <ul class="contact-info">
                                    <li><i class="fa fa-phone"></i> +91 8800435387 </li>
                                    <li><i class="fa fa-envelope"></i> info@plottorace.com</li>
                                    <li><i class="fa fa-home"></i> Street No 22, Vipin Garden Extension, Dwarka Mor, New Delhi</li>
                                    <li><i class="fa fa-home"></i> Startx Co-working Space, Opposite Dlf Cyber City Gate No. 2, Gachibowli, Hyderabad</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-2 col-sm-6">
                            <div class="widget links-widget">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">OPC</a></li>
                                    <li><a href="#">LLP</a></li>
                                    <li><a href="ContactUs.aspx">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-3 col-sm-6">
                            <div class="widget support-widget">
                                <h3>Support</h3>
                                <ul>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Free Consultation with CA</a></li>
                                    <li><a href="#">Free consultation with CS</a></li>
                                    <li><a href="#">Free Consultation with Lawyer</a></li>
      
                                    <li><a href="#">Professional Services</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                        <%--<div class="col col-md-3 col-sm-6">
                            <div class="widget twitter-feed-widget">
                                <h3>Twitter Feed</h3>
                                <ul>
                                    <li>
                                        <div class="text">
                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. Ed quia con sequuntur magni dolores.</p>
                                        </div>
                                        <div class="info-box">
                                            <i class="fa fa-twitter"></i>
                                            <strong><a href="#">@Mark Wahlberg</a></strong>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text">
                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. Ed quia con sequuntur magni dolores.</p>
                                        </div>
                                        <div class="info-box">
                                            <i class="fa fa-twitter"></i>
                                            <strong><a href="#">@Mark Wahlberg</a></strong>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end upper-footer -->--%>
            <div class="copyright-info">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="copyright-area">
                             
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="footer-social">
                                <span>Follow us: </span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->        
    <!--</div>-->
   


    <!-- All JavaScript files
    ================================================== -->
    <script src="Javascript/jquery.min.js"></script>
    <script src="Javascript/bootstrap.js"></script>

    <!-- Plugins for this template -->
    <script src="Javascript/jquery-plugin-collection.js"></script>

    <!-- Google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key"></script>

    <!-- Custom script for this template -->
    <script src="Javascript/script.js"></script>
</body>
</html>