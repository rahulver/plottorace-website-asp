﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Trademark.aspx.cs" Inherits="Trademark" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in India.">
    <meta name="author" content="Best Legal Service Provider in Hyderabad.">
    <meta name="author" content="Best Legal Service Provider in New Delhi">
    <meta name="author" content="Best Legal Service Provider in Hajipur.">
    <meta name="author" content="Best Legal Service Provider in Kolkata.">
    <meta name="author" content="Company Secretaries in Hyderabad.">
    <meta name="author" content="Company Secretaries in New Delhi.">
    <meta name="author" content="Company Secretaries in India.">
    <meta name="author" content="Company Secretaries in Kolkata.">
    <meta name="author" content="Company Secretaries in Hajipur.">

    <!-- Page Title -->
    <title> PlottoRace Consultancy- Best Corporate Legal Service Provider in India. </title>

    <!-- Favicon and Touch Icons -->
    <link href="assets/images/favicon/favicon.png" rel="shortcut icon" type="image/png">
    <link href="assets/images/favicon/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
    <link href="assets/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="assets/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="assets/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icon fonts -->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/flaticon.css" rel="stylesheet" />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" />

    <!-- Plugins for this template -->
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/owltheme.css" rel="stylesheet" />
    <link href="css/slick.css" rel="stylesheet" />
    <link href="css/slick-theme.css" rel="stylesheet" />
    <link href="css/owl-transition.css" rel="stylesheet" />
    <link href="css/fancybox.css" rel="stylesheet" />
    <link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/StyleSheet1.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet" />
</head>

<body>

        <!-- Start header -->
        <header id="header" class="site-header header-style-1">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-sm-6">
                            <ul class="contact-info">
                                <li><i class="fa fa-phone-square"></i> Phone: +91 8800435387</li>
                                <li><i class="fa fa-clock-o"></i> Mon - Fri: 9 am - 7 pm</li>
                            </ul>
                        </div>
                        <div class="col col-sm-6">
                            <div class="language">
                                <span><i class="fa fa-globe"></i> Lang:</span>
                                <div class="select-box">
                                    <select class="selectpicker" id="language-select">
                                        <option>Eng</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> <!-- end topbar -->
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand">
                            <img src="Images/Logo_.jpg" height="75" width="250" /></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">

                            <li><a href="Default.aspx">Home</a></li>

                            <li><a href="#">About</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Services</a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Start Your own Business</a>
                                        <ul class="sub-menu">
                                            <li><a href="OPC.aspx">Registration of One Person Company(OPC)</a></li>
                                            <li><a href="LLP.aspx">Registration of LLP</a></li>
                                            <li><a href="PublicLimitedCompany.aspx">Registration of Public Limited Company </a></li>
                                            <li><a href="PrivateLimitedCompany.aspx">Registration of Private Limited Company</a></li>
                                            <li><a href="Section8.aspx">Registration of Section 8</a></li>

                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Company Law</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">ROC/Annual filing</a></li>
                                            <li><a href="#">Director Appointment/Resignation</a></li>
                                            <li><a href="#">Increase in Share Capital </a></li>
                                            <li><a href="#">Alteration of MOA</a></li>
                                            <li><a href="#">Name Change</a></li>
                                            <li><a href="#">Minutes Book</a></li>
                                            <li><a href="#">Share Transfer</a></li>
                                            <li><a href="#">Share Certificate </a></li>
                                            <li><a href="#">Share Holder Register</a></li>
                                            <li><a href="#">Registered Office Change</a></li>
                                            <li><a href="#">Charge Creation/Satisfaction and Modification</a></li>
                                            <li><a href="#">Strike Off</a></li>fd

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Taxtion</a>
                                <ul class="sub-menu">
                                    <li><a href="Income_Tax_Return.html">Income Tax Return</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">GST</a>
                                        <ul class="sub-menu">
                                            <li><a href="GST.aspx">GST</a></li>
                                            <li><a href="GSTDocuments.aspx">GST DOC</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="Other_Tax_compliances.html">Other Tax compliances</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">IPR</a>
                                <ul class="sub-menu">
                                    <li><a href="Trademark.aspx">Trademark</a></li>
                                    <li><a href="Copyright.aspx">Copyright</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Other Services</a>
                                <ul class="sub-menu">
                                    <li><a href="#">ISO Certification</a></li>
                                    <li><a href="MSME.aspx">MSME/SSI Registration</a></li>
                                    <li><a href="Patent.aspx">Patent</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                    <div class="search-social">
                        <div class="header-search-area">
                            <div class="header-search-form">
                                <%--<form class="form">
                                    <div>
                                        <input type="text" class="form-control" placeholder="Search here">
                                    </div>
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>--%>
                            </div>
                            <div>
                                <button class="btn open-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
    <!--start of content here-->
    <div style="position:relative; text-align:center; color:white;">
    <img src="Images/Services/Trademark.jpg" height="250" width="1550" />
<%--<div style="position:absolute; top:50%; left:50%; transform: translate(-50%, -50%); font-size:60px; color:black;"><b>Private Limited Company</b></div>--%>--%>
    </div>
    <div style="background-color:white;">
        <div class="heading">
            <b>Trademark</b> 
        </div>
            <div class="heading-content">
                <div class="heading-content">
                    <h3> <b style="color:green;">Definition and Significance</b></h3>
                    <h4>What is Trademark?</h4>
                    <p style="text-align:justify;">A trademark means a mark capable of being represented graphically and which is capable of distinguishing the goods or services of one undertaking from those of other undertakings.</p>
                    <p style="text-align:justify;">A trademark can be a sign, words, letters, numbers, drawings, pictures, emblem, colours or combination of colours, shape of goods, graphic representation or packaging or sound or any combination of the above as applied to goods or services.</p>
                    <h4>MINISTRY/OFFICE ADMINISTERING THE IPR:</h4>
                    <ul>
                        <li>Department of Industrial Policy and Promotion, Ministry of Commerce & Industry</li>
                        <li>Controller General of Patents, Designs and Trade Marks</li>
                        <li>Concerned IP Act: Trade Marks Act 1999 (as amended in 2010)</li>
                        </ul>
                     <h3><b style="color:green;">Documents Required for filing trademark application:</b></h3>
                
                    <ul style="text-align:justify;">
                       <li>The name, address and nationality of the applicant. If the applicant is a partnership firm, the names of all the partners. Also mention whether any minor is a partner.
                        </li>
                        
                        <li>If the applicant is a company, the country or state of incorporation.
                        </li>
                        <li>A list of goods and/or services for which registration is required.
                            </li>
                            
                           <li> Soft copy of the trademark to be registered.
                        </li>
                        <li>If the mark contains or consists of non-English words, a translation of those words into English is required.
                            </li>
                        <li>If the application is to claim priority from an earlier filed convention application, details of that application is also required (application number, filing date, country and goods/services). A certified priority document or its duly notarized copy is to be submitted. If the certificate is not in English, a certified/notarized English translation is also required. If it is not readily available, the application can be filed based on the basic application number, date of the application and country of the application. A copy of the priority document can be submitted within 1 month from the filing date of the application.<li>
                        <li>Date of first use of the trademark in India, if at all used.</li> 
                         <li>Power of attorney simply signed by the applicant (no legalization or notarization is required). For Indian clients, power of attorney to be executed in 100 Rs. stamp paper and signed by the applicant. The power of attorney is not required at the time of lodging the application and can be submitted later with no additional cost.
                         </li>
                        </ul>
                    </div>
                </div>
        </div>
                  
    <br/>
    <br/>
       

        <!-- start site-footer -->
        <footer class="site-footer">
            <div class="upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-4 col-sm-6">
                            <div class="widget about-widget">
                                <div class="footer-logo"><img src="assets/images/footer-logo.png" alt></div>
                                <p>We are one of the Indis's leading corporate consultancy firm having experience of serving more than 100+ corporate clients.</p>
                                <ul class="contact-info">
                                    <li><i class="fa fa-phone"></i> +91 8800435387 </li>
                                    <li><i class="fa fa-envelope"></i> info@plottorace.com</li>
                                    <li><i class="fa fa-home"></i> Street No 22, Vipin Garden Extension, Dwarka Mor, New Delhi</li>
                                    <li><i class="fa fa-home"></i> Startx Co-working Space, Opposite Dlf Cyber City Gate No. 2, Gachibowli, Hyderabad</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-2 col-sm-6">
                            <div class="widget links-widget">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">OPC</a></li>
                                    <li><a href="#">LLP</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col col-md-3 col-sm-6">
                            <div class="widget support-widget">
                                <h3>Support</h3>
                                <ul>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Free Consultation with CA</a></li>
                                    <li><a href="#">Free consultation with CS</a></li>
                                    <li><a href="#">Free Consultation with Lawyer</a></li>
      
                                    <li><a href="#">Professional Services</a></li>
                                </ul>
                            </div>
                        </div>

                      
            <div class="copyright-info">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="copyright-area">
                             
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="footer-social">
                                <span>Follow us: </span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->        
    <!--</div>-->
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="Javascript/jquery.min.js"></script>
    <script src="Javascript/bootstrap.js"></script>

    <!-- Plugins for this template -->
    <script src="Javascript/jquery-plugin-collection.js"></script>

    <!-- Google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key"></script>

    <!-- Custom script for this template -->
    <script src="Javascript/script.js"></script>
</body>
</html>



